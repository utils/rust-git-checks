// Copyright Kitware, Inc.
//
// Licensed under the Apache License, Version 2.0 <LICENSE-APACHE or
// http://www.apache.org/licenses/LICENSE-2.0> or the MIT license
// <LICENSE-MIT or http://opensource.org/licenses/MIT>, at your
// option. This file may not be copied, modified, or distributed
// except according to those terms.

#![warn(missing_docs)]
// XXX(rust-1.66)
#![allow(clippy::uninlined_format_args)]

//! Git checks
//!
//! There are many things in git repositories which can be checked mechanically such as whitespace
//! errors, submodule availability, eligibility for a branch, and more. This crate provides traits
//! for these checks and a set of common checks which operate with minimal file inspection.

mod check;
mod commit;
mod context;
pub mod impl_prelude;
mod run;
mod utils;

pub use check::BranchCheck;
pub use check::Check;
pub use check::CheckResult;
pub use check::ContentCheck;
pub use check::Severity;
pub use check::TopicCheck;
pub use commit::Commit;
pub use commit::CommitError;
pub use commit::Content;
pub use commit::DiffInfo;
pub use commit::FileName;
pub use commit::FileNameError;
pub use commit::StatusChange;
pub use commit::Topic;
pub use context::AttributeError;
pub use context::AttributeState;
pub use context::CheckGitContext;
pub use run::GitCheckConfiguration;
pub use run::RunError;

#[cfg(test)]
mod test;
