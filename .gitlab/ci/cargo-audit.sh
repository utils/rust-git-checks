#!/bin/sh

set -e

readonly version="0.21.0"
readonly sha256sum="1afe6f3dba2e3d81dfb8db34be37ae9647480b132cd719357276ae643ebf962a"
readonly basename="cargo-audit-x86_64-unknown-linux-musl-v$version"
readonly filename="$basename.tgz"

cd .gitlab

echo "$sha256sum  $filename" > cargo-audit.sha256sum
curl -OL "https://github.com/rustsec/rustsec/releases/download/cargo-audit%2Fv$version/$filename"
sha256sum --check cargo-audit.sha256sum
tar --strip-components=1 -xf "$filename" "$basename/cargo-audit"
chmod +x cargo-audit
